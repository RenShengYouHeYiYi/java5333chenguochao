public class Test {
    public static void main(String args[]) {
	CPU cpu = new CPU();
	HardDisk disk = new HardDisk();
	cpu.setSpeed(2200);
	disk.setAmount(200);
	PC pc = new PC();
	pc.setCPU(cpu);
	pc.setHardDisk(disk);
	pc.show();

	System.out.println(cpu.toString());
	System.out.println(disk.toString());
	System.out.println(pc.toString());
	
    }
}
