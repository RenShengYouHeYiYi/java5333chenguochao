 public class lizi1 {
     public static void main(String [] args){
         int arr[] = {1,2,3,4,5,6,7,8};

         for(int i:arr){
             System.out.print(i + " ");
         }
         System.out.println();

         for(int i=5; i<arr.length; i++)
             arr[i-1] = arr[i];

         arr[7] = 0;

         for(int i:arr){
             System.out.print(i + " ");
         }
         System.out.println();

         for(int i=arr.length; i>4; i--)
             arr[i-1] = arr[i-2];

         arr[4] = 5;

         for(int i:arr){
             System.out.print(i + " ");
         }
         System.out.println();

     }
 }
