import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.Key;

public class ServerFour {
    public static void main(String args[]) {
        ServerSocket serverForClient = null;
        Socket socketOnServer = null;
        DataOutputStream out = null;
        DataInputStream in = null;
        try {
            serverForClient = new ServerSocket(5333);
        } catch (IOException e1) {
            System.out.println(e1);
        }
        try {
            System.out.println("等待客户呼叫");
            socketOnServer = serverForClient.accept(); //堵塞状态，除非有客户呼叫
            System.out.println("客户已连接");
            out = new DataOutputStream(socketOnServer.getOutputStream());
            in = new DataInputStream(socketOnServer.getInputStream());

            Key_DH.fun("Spub.txt","Spri.txt");
            int len = Integer.parseInt(in.readUTF());
            byte np[] = new byte[len];
            for (int i = 0;i<len;i++) {
                String temp = in.readUTF();
                np[i] = Byte.parseByte(temp);
            }
            ObjectInputStream ois = new ObjectInputStream (new ByteArrayInputStream(np));
            Key k2 = (Key)ois.readObject();;
            FileOutputStream f2 = new FileOutputStream("Cpub.txt");
            ObjectOutputStream b2 = new ObjectOutputStream(f2);
            b2.writeObject(k2);

            FileInputStream fp = new FileInputStream("Spub.txt");
            ObjectInputStream bp = new ObjectInputStream(fp);
            Key kp = (Key) bp.readObject();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(kp);
            byte[] kb = baos.toByteArray();
            out.writeUTF(kb.length + "");
            for (int i = 0; i < kb.length; i++) {
                out.writeUTF(kb[i] + "");
            }

            KeyAgree.fun("Cpub.txt","Spri.txt");

            String leng = in.readUTF(); // in读取信息，堵塞状态
            byte ctext[] = new byte[Integer.parseInt(leng)];
            for (int i = 0;i<Integer.parseInt(leng);i++) {
                String temp = in.readUTF();
                ctext[i] = Byte.parseByte(temp);
            }
            String check = in.readUTF();
            // 获取密钥
            FileInputStream f = new FileInputStream("sb.txt");
            byte[] keysb = new byte[24];
            f.read(keysb);
            System.out.println("public key：");
            for (int i = 0;i<24;i++) {
                System.out.print(keysb[i]+",");
            }
            System.out.println("");
            SecretKeySpec k = new SecretKeySpec(keysb, "DESede");
            // 解密
            Cipher cp = Cipher.getInstance("DESede");
            cp.init(Cipher.DECRYPT_MODE, k);
            byte[] ptext = cp.doFinal(ctext);
            System.out.println("后缀表达式已被解密：");
            for (int i = 0; i < ptext.length; i++) {
                System.out.print(ptext[i] + ",");
            }
            System.out.println("");
            // 显示明文
            String p = new String(ptext, "UTF8");
            String pMd5 = DigestPass.fun(p);
            System.out.println("解密后明文的MD5值："+pMd5);
            if (pMd5.equals(check)){
                System.out.println("和客户端的MD5值一致");
                System.out.println("服务器收到客户的请求:计算后缀表达式" + p);
                out.writeUTF(MyDC.jisuan(new StringBuffer(p))+"");
            }
            else {
                System.out.println("警告：和客户端的MD5值不一致！");
            }
        } catch (Exception e) {
            System.out.println("客户已断开" + e);
        }
    }
}
