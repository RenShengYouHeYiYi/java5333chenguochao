public class MyBC {
    public static StringBuffer toPostfix(StringBuffer s2) {
        StringBuffer r = new StringBuffer();
        StringBuffer t = new StringBuffer();
        int i = 0;
        int p = 0;
        while (i < s2.length()) {
            if (Character.isDigit(s2.charAt(i))) {
                while (Character.isDigit(s2.charAt(i))) {
                    r.append(s2.charAt(i++));
                    if (s2.length() == i) {
                        break;
                    }
                }
                r.append(' ');
                continue;
            }

            if (s2.charAt(i) == '(') {
                t.append(s2.charAt(i++));
                p++;
                continue;
            }

            if (s2.charAt(i) == ' ') {
                i++;
                continue;
            }

            if (s2.charAt(i) == ')') {
                if (t.charAt(p - 1) != '(') {
                    while (t.charAt(p - 1) != '(') {
                        if (t.charAt(p - 1) == '+' || t.charAt(p - 1) == '-' || t.charAt(p - 1) == '*' || t.charAt(p - 1) == '/') {
                            r.append(t.charAt(p - 1));
                            r.append(' ');
                            t.deleteCharAt(p - 1);
                            p--;
                        }
                    }
                }
                t.deleteCharAt(p - 1);
                p--;
                i++;
                continue;
            }

            if (s2.charAt(i) == '+' || s2.charAt(i) == '-' || s2.charAt(i) == '*' || s2.charAt(i) == '/') {
                if (p != 0 && (s2.charAt(i) == '+' || s2.charAt(i) == '-')) {
                    while (t.length() != 0 && t.charAt(p - 1) != '(') {
                        if (t.charAt(p - 1) == '+' || t.charAt(p - 1) == '-' || t.charAt(p - 1) == '*' || t.charAt(p - 1) == '/') {
                            r.append(t.charAt(p - 1));
                            r.append(' ');
                            t.deleteCharAt(p - 1);
                            p--;
                        }
                    }
                }
                if (p != 0 && (s2.charAt(i) == '*' || s2.charAt(i) == '/')) {
                    while (t.length() != 0 && t.charAt(p - 1) != '(' && t.charAt(p - 1) != '+' && t.charAt(p - 1) != '-') {
                        if (t.charAt(p - 1) == '*' || t.charAt(p - 1) == '/') {
                            r.append(t.charAt(p - 1));
                            r.append(' ');
                            t.deleteCharAt(p - 1);
                            p--;
                        }
                    }

                }
                t.append(s2.charAt(i));
                p++;
                i++;
                continue;
            }

        }
        while (t.length() != 0 && p != 0) {
            r.append(t.charAt(p - 1));
            r.append(' ');
            t.deleteCharAt(p - 1);
            p--;
        }
        r.deleteCharAt(r.length() - 1);
        return r;
    }
}
