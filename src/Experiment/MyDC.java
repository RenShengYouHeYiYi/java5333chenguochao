import java.util.Stack;

public class MyDC {
    public static int jisuan(StringBuffer s) {
        Stack q = new Stack();
        Stack temp = new Stack();
        int i = 0, a, b;
        int result;
        char c;
        for (i = s.length() - 1; i >= 0; i--) {
            if (s.charAt(i) != ' ') {
                q.push(s.charAt(i));
            }
        }
        while (true) {
            c = (char) q.peek();
            if (isNum(c)) {
                temp.push(q.pop());
            } else {
                a = Integer.parseInt(temp.pop()+"");
                b = Integer.parseInt(temp.pop()+"");
                c = (char) q.pop();
                switch (c) {
                    case '+':
                        a = a + b;
                        break;
                    case '-':
                        a = b - a;
                        break;
                    case '*':
                        a = a * b;
                        break;
                    case '/':
                        a = b / a;
                        break;
                    default:
                        System.out.println("Error:" + c);
                }
                temp.push(a);
                if (q.empty()) {
                    break;
                }
            }
        }
        result = Integer.parseInt(temp.pop()+"");
        return result;
    }

    public static boolean isNum(char c) {
        if (48 <= c && c <= 57) {
            return true;
        } else {
            return false;
        }
    }
}


