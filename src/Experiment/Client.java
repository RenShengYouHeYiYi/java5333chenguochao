import java.io.*;
import java.net.*;
import java.util.Scanner;

public class Client {
    public static void main(String args[]) {
        Socket mysocket;
        DataInputStream in = null;
        DataOutputStream out = null;
        Scanner scanner = new Scanner(System.in);
        String str;
        try {
            mysocket = new Socket("192.168.253.1", 5333);
            in = new DataInputStream(mysocket.getInputStream());
            out = new DataOutputStream(mysocket.getOutputStream());
            System.out.println("已连接服务器，请输入中缀表达式：");
            str = scanner.nextLine();
            out.writeUTF(MyBC.toPostfix(new StringBuffer(str)).toString());
            String s = in.readUTF();   //in读取信息，堵塞状态
            System.out.println("客户收到服务器的回应:" + s);
        } catch (Exception e) {
            System.out.println("服务器已断开" + e);
        }
    }
}
