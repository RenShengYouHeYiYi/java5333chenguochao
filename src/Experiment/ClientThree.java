import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.net.Socket;
import java.security.Key;
import java.util.Scanner;
public class ClientThree {
    public static void main(String args[]) {
        Socket mysocket;
        DataInputStream in = null;
        DataOutputStream out = null;
        Scanner scanner = new Scanner(System.in);
        String str;
        try {
            mysocket = new Socket("192.168.253.1", 5333);
            in = new DataInputStream(mysocket.getInputStream());
            out = new DataOutputStream(mysocket.getOutputStream());
            System.out.println("已连接服务器...");
            System.out.println("请输入明文（中缀表达式）");
            str = scanner.nextLine();
            Key_DH.fun("Cpublic.txt","Cpriblic.txt");
            FileInputStream fp = new FileInputStream("Cpublic.txt");
            ObjectInputStream bp = new ObjectInputStream(fp);
            Key kp = (Key) bp.readObject();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(kp);
            byte[] kb = baos.toByteArray();
            out.writeUTF(kb.length + "");
            for (int i = 0; i < kb.length; i++) {
                out.writeUTF(kb[i] + "");
            }
            Thread.sleep(500);
            int len = Integer.parseInt(in.readUTF());
            byte np[] = new byte[len];
            for (int i = 0;i<len;i++) {
                String temp = in.readUTF();
                np[i] = Byte.parseByte(temp);
            }
            ObjectInputStream ois = new ObjectInputStream (new ByteArrayInputStream (np));
            Key k2 = (Key)ois.readObject();;
            FileOutputStream f2 = new FileOutputStream("Spub.txt");
            ObjectOutputStream b2 = new ObjectOutputStream(f2);
            b2.writeObject(k2);
            KeyAgree.fun("Spublic.txt","Cpriblic.txt");
            FileInputStream f = new FileInputStream("sb.txt");
            byte[] keysb = new byte[24];
            f.read(keysb);
            System.out.println("public key：");
            for (int i = 0;i<24;i++) {
                System.out.print(keysb[i]+",");
            }
            System.out.println("");
            SecretKeySpec k = new SecretKeySpec(keysb, "DESede");
            Cipher cp = Cipher.getInstance("DESede");
            cp.init(Cipher.ENCRYPT_MODE, k);

            byte ptext[] = MyBC.toPostfix(new StringBuffer(str)).toString().getBytes("UTF-8");
            byte ctext[] = cp.doFinal(ptext);
            System.out.println("加密后的后缀表达式：");
            for (int i = 0; i < ctext.length; i++) {
                System.out.print(ctext[i] + ",");
            }
            System.out.println("");
            out.writeUTF(ctext.length + "");
            for (int i = 0; i < ctext.length; i++) {
                out.writeUTF(ctext[i] + "");
            }
            String s = in.readUTF();   //in读取信息，堵塞状态
            System.out.println("客户收到服务器的回应:" + s);
        } catch (Exception e) {
            System.out.println("服务器已断开" + e);
        }
    }
}
