class Complex {
    private double r;
    private double i;

    public Complex(double r, double i) {
        this.r = r;
        this.i = i;
    }
    public static double getRealPart(double r) {
        return r;
    }
    public static double getImagePart(double i) {
        return i;
    }

    public Complex ComplexAdd(Complex a) {
        return new Complex(r + a.r, i + a.i);
    }

    public Complex ComplexSub(Complex a) {
        return new Complex(r - a.r, i - a.i);
    }
    public Complex ComplexMulti(Complex a) {
        return new Complex(r * a.r - i * a.i , r * a.i + i * a.r);
    }
    public Complex ComplexDiv(Complex a) {
        return new Complex((r * a.i + i * a.r) / (a.i * a.i + a.r * a.r), (i * a.i + r * a.r) / (a.i * a.i + a.r * a.r));
    }
    public String toString() {
        String s =" ";
        if (i > 0)
            s =r +"+"+ i +"i";
        if (i == 0)
            s =r+"";
        if (i < 0)
            s = r +" "+ i+"i";
        return s;
    }

